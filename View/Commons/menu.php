<nav>
    <label for="menu-mobile" class="menu-mobile"> Menu</label>
    <input type="checkbox" id="menu-mobile" role="bouton">
    <ul>
        <li class="menu-rmga"><a href="#">R.M.G.A</a>
            <ul class="sous-menu">
                <li><a href="<?= URL ?>association">Qui sommes-nous ?</a></li>
                <li><a href="<?= URL ?>partenaires">Partenaires</a></li>
            </ul>
        </li>
        <li class="menu-pension"><a href="#">Pensionnaires</a>
            <ul class="sous-menu">
                <li><a href="<?= URL ?>pensionnaires&idstatut=<?= ID_STATUT_A_L_ADOPTION ?>">Ils cherchent une
                        famille</a></li>
                <li><a href="<?= URL ?>pensionnaires&idstatut=<?= ID_STATUT_FALD ?>">Famille d'Accueil Longue Durée</a>
                </li>
                <li><a href="<?= URL ?>pensionnaires&idstatut=<?= ID_STATUT_ADOPTE ?>">Les anciens</a></li>
                <?php if(Securite::verifAccessSession()){ ?>

                <li> <a href="<?= URL ?>genererPensionnaireAdmin">Gestion des pensionnaires</a>
                </li>

                <?php } ?>
            </ul>
        </li>

        <li class="menu-actu"><a href="#">Actualité</a>
            <ul class="sous-menu">
                <li><a href="<?= URL ?>actus&type=<?= TYPE_NEWS ?>">Nouvelles des adoptés</a></li>
                <li><a href="<?= URL ?>actus&type=<?= TYPE_EVENTS ?>">Evénements</a>
                </li>
                <li><a href="<?= URL ?>actus&type=<?= TYPE_ACTIONS ?>">Nos actions au quotidien</a></li>
                <?php if(Securite::verifAccessSession()){ ?>

                <li><a href="<?= URL ?>genererNewsAdmin">Gestion des new</a> </li>

                <?php } ?>
            </ul>
        </li>

        <li class="menu-conseil"><a href="#"> Conseils</a>
            <ul class="sous-menu">
                <li><a href="<?= URL ?>temperatures">Températures</a></li>
                <li><a href="<?= URL ?>chocolat">Le chocolat</a>
                </li>
                <li><a href="<?= URL ?>plantes">Les plantes toxiques</a></li>
                <li><a href="<?= URL ?>sterilisation">Stérilisation</a></li>
                <li><a href="<?= URL ?>educateur">Educateur canin</a></li>
            </ul>
        </li>


        <li class="menu-contacts"><a href="#"> Contacts</a>
            <ul class="sous-menu">
                <li><a href="<?= URL ?>contact">Contact</a></li>
                <li><a href="<?= URL ?>don">Don</a>
                </li>
                <li><a href="<?= URL ?>mentions">Mentions légales</a></li>

            </ul>
        </li>

    </ul>
</nav>