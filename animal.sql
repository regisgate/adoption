-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 06 août 2021 à 15:13
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `nana`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualite`
--

DROP TABLE IF EXISTS `actualite`;
CREATE TABLE IF NOT EXISTS `actualite` (
  `id_actualite` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_actualite` varchar(50) NOT NULL,
  `contenu_actualite` longtext NOT NULL,
  `date_publication_actualite` date NOT NULL,
  `id_image` int(11) NOT NULL,
  `id_type_actualite` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_actualite`),
  KEY `FK_image_actualite` (`id_image`),
  KEY `FK_type_actualite` (`id_type_actualite`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `actualite`
--

INSERT INTO `actualite` (`id_actualite`, `libelle_actualite`, `contenu_actualite`, `date_publication_actualite`, `id_image`, `id_type_actualite`) VALUES
(1, 'Actu de Framboise 2', 'Un petit coucou de notre doyenne CHIPIE (20ans) en famille d accueil longue dur&eacute;e chez notre tr&eacute;sori&egrave;re.\r\nLa miss a un programme journalier surcharg&eacute; :\r\nManger, dormir, petit tour &agrave; la liti&egrave;re, retour dans le dodo juste devant le chauffage, c&acirc;lins &agrave; ma Tatynou pour qu elle me donne des friandises et re dodo. Merci &agrave; tous pour les petits cadeaux, elle appr&eacute;cie la gourmande\r\n\r\nBonne fin de soir&eacute;e &agrave; tous et &agrave; bient&ocirc;t pr la suite des aventures de mamie Chipie', '2019-05-14', 1, 3),
(2, 'Actu 2', 'Petit rappel rapide de l’histoire.\r\n\r\nLundi une de nos amies, Cynthia, trouve cette petite misère sur la route devant des poubelles.\r\nDirection vétérinaire, son pronostic vital est engagé nous décidons de tenter le coup.\r\nAveugle, une otite énorme, une patte « morte » des griffes enfoncées dans les coussinets, une hypothermie et un poids de 1kg500... \r\n\r\nMathilde, notre adorable vétérinaire nous dit qu\'elle est vieille, proche des 20 ans.\r\nVendredi, un monsieur se met en relation avec nous et explique qu’il s’agit de Chipie 20 ans, la chatte de sa maman qui est malheureusement décédée.\r\nNe pouvant pas la prendre chez lui à cause de son chien et ne voulant pas la changer de lieu à son âge elle reste dans la maison inhabitée et ils viennent la voir chaque jour. Par contre nous ne savons pas expliquer comment Chipie s\'est retrouvée devant cette poubelle ! Elle avait disparu du jardin et ils ont cru qu\'elle était partie mourir quelque part... \r\n\r\nEn accord avec eux, Chipie restera à l\'association et plus particulièrement chez Karine notre trésorière où elle a son petit coin douillet à côté du radiateur et beaucoup de câlins et d\'amour. \r\n\r\nElle a repris un peu de poids, ses otites guérissent doucement, ses pattes cicatrisent bien mais voilà CHIPIE a 20 ans, son petit corps est fatigué, elle dort énormément. Semaine prochaine elle revoit Mathilde notre vétérinaire pour faire un point. \r\n\r\nNous allons la chouchouter un maximum et veiller à son bien-être jusqu’au dernier moment. Nourriture à volonté (viande crue, croquettes ramollies, sachets fraîcheur, etc) un petit coin bien chaud et beaucoup beaucoup d\'amour. \r\n\r\nMerci à TOUS pour votre confiance, à son ancienne famille qui accepte que Chipie termine sa vie avec nous, à Karine sa Tatynou, Laure pour les soins, Cynthia pour le sauvetage, le cabinet vétérinaire Debailleul et sa super équipe, etc etc \r\n\r\nVoilà notre petite douceur de Noël', '2019-05-06', 2, 1),
(13, 'Une news', 'test de news', '2019-06-09', 53, 3),
(14, 'testAvecImageDuSite', 'testAvecImageDuSite', '2019-06-10', 4, 1),
(15, 'test', 'test', '2019-06-10', 53, 1);

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `login` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`login`, `password`) VALUES
('admin', '$2y$10$OE6tMGyrwan3ZNo9IwIgquV9b7.Md.H1gBCsahNDDSZSDjzbe6tU6'),
('regisgate', 'mamanmarie');

-- --------------------------------------------------------

--
-- Structure de la table `animal`
--

DROP TABLE IF EXISTS `animal`;
CREATE TABLE IF NOT EXISTS `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `nom_animal` varchar(50) NOT NULL,
  `type_animal` enum('Chien','Chat') NOT NULL,
  `puce` varchar(50) DEFAULT NULL,
  `sexe` tinyint(1) NOT NULL,
  `date_naissance_animal` date NOT NULL,
  `date_adoption_animal` date DEFAULT NULL,
  `ami_chien` enum('oui','non','N/A') NOT NULL,
  `ami_chat` enum('oui','non','N/A') NOT NULL,
  `ami_enfant` enum('oui','non','N/A') NOT NULL,
  `description_animal` longtext,
  `adoption_desc_animal` longtext,
  `localisation_animal` longtext,
  `engagement_animal` longtext,
  `id_statut` int(11) NOT NULL,
  PRIMARY KEY (`id_animal`),
  KEY `FK_statut_animal` (`id_statut`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `animal`
--

INSERT INTO `animal` (`id_animal`, `nom_animal`, `type_animal`, `puce`, `sexe`, `date_naissance_animal`, `date_adoption_animal`, `ami_chien`, `ami_chat`, `ami_enfant`, `description_animal`, `adoption_desc_animal`, `localisation_animal`, `engagement_animal`, `id_statut`) VALUES
(1, 'Framboise', 'Chien', '', 0, '2019-05-16', NULL, 'oui', 'oui', 'oui', 'Nous voulons encore et encore sensibiliser les gens sur les cons&eacute;quences de la NON St&eacute;rilisation. &lt;br /&gt;\r\nLa petite FRAMBOISE &acirc;g&eacute;e aujourd&rsquo;hui de 6 semaines est malheureusement encore un exemple des cons&eacute;quences de celle-ci.&lt;br /&gt;\r\nSa maman, chatte de ferme non st&eacute;rilis&eacute;e, a eu des rapports avec son propre fils ?\r\nLe r&eacute;sultat est l&agrave;, un chaton mort-n&eacute; totalement diforme, 2 chatons qui vont bien et la petite FRAMBOISE.\r\nElle a eu du mal &agrave; grandir, puis les jours passent et nous avons un doute sur sa vision.\r\nVendredi nous d&eacute;cidons de la montrer &agrave; notre v&eacute;t&eacute;rinaire-ophtalmologue, puis le verdict se confirme : malvoyante voire aveugle.\r\nNous referons des examens d\'ici 3 semaines mais une chose est certaine, elle ne verra pas normalement. \r\n\r\nAlors oui ce n\'est pas mortel mais cela aurait pu &ecirc;tre &eacute;vit&eacute;.\r\nFramboise pourra t-elle trouver une famille qui l&rsquo;aimera malgr&eacute; son handicap !? Nous l&rsquo;esp&eacute;rons car elle se d&eacute;brouille aussi bien que son fr&egrave;re et sa soeur. \r\n\r\nVoil&agrave; par piti&eacute; il faut absolument faire st&eacute;riliser vos animaux, assumer jusqu&rsquo;au bout votre choix d\'avoir un animal. \r\nNON il n\'est pas n&eacute;cessaire qu&rsquo;une chatte connaisse la maternit&eacute;. \r\nNON ce n\'est pas vrai qu\'une chatte st&eacute;rilis&eacute;e chasse moins vos souris.\r\nVous allez la prot&eacute;ger des maladies, des dangers de la gestation etc.\r\nLa st&eacute;rilisation n&rsquo;a que du POSITIF \r\n\r\nPour info sa maman est maintenant st&eacute;rilis&eacute;e mais combien ne vont pas l&rsquo;&ecirc;tre !? Combien vont devoir encore subir l\'irresponsabilit&eacute; de l&rsquo;homme !? \r\n\r\n&Agrave; FRAMBOISE et toutes les petites victimes de la n&eacute;gligence humaine.', '', '', '', 2),
(2, 'Mona2', 'Chat', '22444', 0, '2019-06-10', '2019-06-10', 'non', 'oui', 'N/A', 'Nous n&rsquo;avons plus de nouvelles des gens souhaitant l adopter du coup nous vous pr&eacute;sentons la miss.\r\nMONA pourra vous rejoindre &agrave; partir du 27 mai, et vous avez la possibilit&eacute; de la r&eacute;server ? ( contrat de r&eacute;servation + versement d&rsquo;arrhes)\r\n\r\nNous vous pr&eacute;sentons la petite MONA sublime petite femelle grise avec le poil mi long. \r\n\r\nLa miss est un petit chaton de NOVA. \r\n\r\nMONA est une boule d amour, mais elle a aussi son petit c&ocirc;t&eacute; &laquo; clown &raquo; qui vous fera bien rire. Elle est propre. \r\n\r\nHabitu&eacute;e aux chiens, chats et enfants.', 'Frais d\'adoption de 60 &euro;\r\nElle est identifi&eacute;e par puce &eacute;lectronique 2502687322..., vermifuge et anti-parasitaire externe administr&eacute;s.\r\nLes vaccins sont &agrave; la demande des adoptants, il faudra ajouter 35 &euro;.\r\nUn certificat v&eacute;t&eacute;rinaire vous sera &eacute;galement remis.\r\nLa st&eacute;rilisation sera OBLIGATOIRE &agrave; l\'&acirc;ge de 6 mois, un ch&egrave;que de caution de 200 &euro; vous sera demand&eacute; (non encaiss&eacute;) puis rendu d&egrave;s r&eacute;ception du certificat v&eacute;t&eacute;rinaire attestant de celle-ci.', 'MONA se trouve sur le secteur de Saint Girons 09. Pas de co-voiturage possible.', 'Nous vous demandons de bien r&eacute;fl&eacute;chir aux engagements que vous allez devoir prendre envers ce petit coeur : il devra vous accompagner dans vos changements de vie pendant les 15 ans minimum &agrave; venir ? Il faudra &eacute;galement pouvoir subvenir &agrave; ses besoins ( nourriture, soins v&eacute;t&eacute;rinaire etc ) Un animal n est pas un jouet.', 1),
(3, 'Fonzie', 'Chien', NULL, 1, '2019-06-04', NULL, 'non', 'non', 'non', 'l sera prêt à vous rejoindre à partir du 15 juin quand il aura 8 semaines. \r\n( Réservation possible après visite,signature d un contrat et versement d’arrhes sur l’adoption)\r\n\r\nFONZIE petit chiot né vers le 20/04/2019, nous ne connaissons pas les parents mais d’après le poids actuel il fera entre 15/20kg adulte. \r\n\r\nIl est adorable, sociable, joueur et câlin. Il adore découvrir et apprendre des nouvelles choses \r\n\r\nApprentissage de la propreté à poursuivre des accidents pourront arriver. Il a pu apprendre les codes canins grâce aux chiens de la F, il faudra donc le poursuivre son apprentissage et l’accompagner dans son éducation comme pour tous les chiots. Rencontre possible avec notre éducatrice canin le jour de l adoption. \r\n\r\nHabitué aux chiens, chats, enfants.', 'Participation aux frais vétérinaires de 100€. \r\nIdentification par puce électronique 250268732..., primo-vaccination,vermifuge et anti-parasitaire externe administrés. \r\n\r\nUn certificat vétérinaire vous sera également remis. \r\n\r\nLa stérilisation sera obligatoire à l\'âge de 6 mois, un chèque de caution de 200 € vous sera demandé (non encaissé) puis détruit dès réception du certificat vétérinaire attestant de celle-ci.', 'FONZIE se trouve sur le secteur de Saint Girons 09 ( environ 10km) Pas de co-voiturage possible', 'Nous vous demandons de bien réfléchir aux engagements que vous allez devoir prendre envers ce petit coeur, il devra vous accompagner dans tous vos changements de vie pendant les 15 ans minimum à venir et il faudra aussi subvenir à ses besoins.\r\nUn animal n\'est pas un jouet.\r\n\r\n', 1),
(4, 'Nova', 'Chat', NULL, 0, '2019-06-16', NULL, 'oui', 'oui', 'oui', NULL, NULL, NULL, NULL, 4),
(19, 'Mina', 'Chien', '', 0, '2019-06-04', NULL, 'oui', 'oui', 'oui', 'Mon chien', 'Mon chien', 'Mon chien', 'Mon chien', 2),
(22, 'animal 1', 'Chat', 'test', 0, '2019-06-12', NULL, 'non', 'non', 'non', 'Un animal', 'Un animal', 'Un animal', 'Un animal', 2),
(23, 'testAdopte4', 'Chat', '', 1, '2019-06-12', '2019-06-10', 'non', 'non', 'non', '', '', '', '', 2);

-- --------------------------------------------------------

--
-- Structure de la table `associe`
--

DROP TABLE IF EXISTS `associe`;
CREATE TABLE IF NOT EXISTS `associe` (
  `id_animal` int(11) NOT NULL,
  `id_actualite` int(11) NOT NULL,
  PRIMARY KEY (`id_animal`,`id_actualite`),
  KEY `FK_associe_actualite` (`id_actualite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `caractere`
--

DROP TABLE IF EXISTS `caractere`;
CREATE TABLE IF NOT EXISTS `caractere` (
  `id_caractere` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_caractere_m` varchar(50) NOT NULL,
  `libelle_caractere_f` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_caractere`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `caractere`
--

INSERT INTO `caractere` (`id_caractere`, `libelle_caractere_m`, `libelle_caractere_f`) VALUES
(3, 'Calme', 'Calme'),
(4, 'Doux', 'Douce'),
(5, 'Gentil', 'Gentille'),
(6, 'Adorable', 'Adorable'),
(7, 'Câlin', 'Câline'),
(8, 'Timide', 'Timide');

-- --------------------------------------------------------

--
-- Structure de la table `contient`
--

DROP TABLE IF EXISTS `contient`;
CREATE TABLE IF NOT EXISTS `contient` (
  `id_image` int(11) NOT NULL,
  `id_animal` int(11) NOT NULL,
  PRIMARY KEY (`id_image`,`id_animal`),
  KEY `FK_contient_animal` (`id_animal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contient`
--

INSERT INTO `contient` (`id_image`, `id_animal`) VALUES
(1, 1),
(2, 2),
(57, 2),
(58, 2),
(59, 2),
(4, 3),
(3, 4),
(45, 19),
(52, 22),
(56, 23);

-- --------------------------------------------------------

--
-- Structure de la table `dispose`
--

DROP TABLE IF EXISTS `dispose`;
CREATE TABLE IF NOT EXISTS `dispose` (
  `id_caractere` int(11) NOT NULL,
  `id_animal` int(11) NOT NULL,
  PRIMARY KEY (`id_caractere`,`id_animal`),
  KEY `FK_animal_dispose` (`id_animal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dispose`
--

INSERT INTO `dispose` (`id_caractere`, `id_animal`) VALUES
(4, 1),
(6, 1),
(3, 2),
(6, 2),
(7, 2),
(3, 19),
(4, 19),
(5, 19),
(3, 22),
(4, 22),
(5, 22),
(3, 23);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_image` varchar(50) NOT NULL,
  `url_image` varchar(100) NOT NULL,
  `description_image` longtext,
  PRIMARY KEY (`id_image`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id_image`, `libelle_image`, `url_image`, `description_image`) VALUES
(1, 'Photo 1 de Framboise', 'animaux/chat/framboise/framboise.jpg', 'Une photo du chat Framboise'),
(2, 'Photo 1 de Mona', 'animaux/chat/mona/mona.jpg', 'Photo 1 de Mona'),
(3, 'Photo 1 de Nova', 'animaux/chat/nova/nova.jpg', 'Photo de nova'),
(4, 'Photo de Fonzie', 'animaux/chien/fonzie/fonzie.jpg', 'Photo de fonzie'),
(45, 'regisgate.png', 'animaux/chien/mina/regisgate.png', 'regisgate.png'),
(46, 'test_fb.png', 'news/test_fb.png', 'test_fb.png'),
(47, 'toto_h2p fond blancCarre.png', 'animaux/chat/toto/regisgate.png', 'regisgate.png'),
(48, 'ttoto2_h2p fond blanc400.png', 'animaux/chat/ttoto2/regisgate.png', 'regisgate.png'),
(49, 'Mona2_fb.png', 'animaux/chat/mona2/Mona2_fb.png', 'Mona2_fb.png'),
(50, 'Mona2_IMG_9946-1024x683.jpg', 'animaux/chat/mona2/Mona2_IMG_9946-1024x683.jpg', 'Mona2_IMG_9946-1024x683.jpg'),
(51, 'Mona2_h2p avec fond blanc.png', 'animaux/chat/mona2/Mona2_h2p avec fond blanc.png', 'Mona2_h2p avec fond blanc.png'),
(52, 'animal 1_H2P.png', 'animaux/chien/animal 1/regisgate.png', 'regisgate.png'),
(53, 'Une news_h2p avec fond blanc.png', 'news/Une news_h2p avec fond blanc.png', 'Une news_h2p avec fond blanc.png'),
(54, 'testAdopte_h2p fond blancCarre.png', 'animaux/chat/testadopte/testAdopte_h2p fond blancCarre.png', 'testAdopte_h2p fond blancCarre.png'),
(55, 'testadopte3_h2p fond blancCarre.png', 'animaux/chat/testadopte3/testadopte3_h2p fond blancCarre.png', 'testadopte3_h2p fond blancCarre.png'),
(56, 'testAdopte4_h2p fond blancCarre.png', 'animaux/chat/testadopte4/testAdopte4_h2p fond blancCarre.png', 'testAdopte4_h2p fond blancCarre.png'),
(57, 'Mona2_IMG_9946-1024x683.jpg', 'animaux/chat/mona2/Mona2_IMG_9946-1024x683.jpg', 'Mona2_IMG_9946-1024x683.jpg'),
(58, 'Mona2_h2p avec fond blanc.png', 'animaux/chat/mona2/Mona2_h2p avec fond blanc.png', 'Mona2_h2p avec fond blanc.png'),
(59, 'Mona2_h2p fond blancCarre.png', 'animaux/chat/mona2/Mona2_h2p fond blancCarre.png', 'Mona2_h2p fond blancCarre.png');

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `id_statut` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_statut` varchar(50) NOT NULL,
  `description_statut` longtext,
  PRIMARY KEY (`id_statut`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`id_statut`, `libelle_statut`, `description_statut`) VALUES
(1, 'A l\'adoption', 'Animal à l\'adoption'),
(2, 'Adopté', 'Animal adopté'),
(3, 'FALD', 'Famille d\'accueil longue durée'),
(4, 'A rejoind les étoiles', 'Animal mort');

-- --------------------------------------------------------

--
-- Structure de la table `type_actualite`
--

DROP TABLE IF EXISTS `type_actualite`;
CREATE TABLE IF NOT EXISTS `type_actualite` (
  `id_type_actualite` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_type_actualite`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_actualite`
--

INSERT INTO `type_actualite` (`id_type_actualite`, `libelle_type`) VALUES
(1, 'News'),
(2, 'Actions'),
(3, 'Events');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `actualite`
--
ALTER TABLE `actualite`
  ADD CONSTRAINT `FK_image_actualite` FOREIGN KEY (`id_image`) REFERENCES `image` (`id_image`),
  ADD CONSTRAINT `FK_type_actualite` FOREIGN KEY (`id_type_actualite`) REFERENCES `type_actualite` (`id_type_actualite`);

--
-- Contraintes pour la table `animal`
--
ALTER TABLE `animal`
  ADD CONSTRAINT `FK_statut_animal` FOREIGN KEY (`id_statut`) REFERENCES `statut` (`id_statut`);

--
-- Contraintes pour la table `associe`
--
ALTER TABLE `associe`
  ADD CONSTRAINT `FK_associe_actualite` FOREIGN KEY (`id_actualite`) REFERENCES `actualite` (`id_actualite`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_associe_animal` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id_animal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `contient`
--
ALTER TABLE `contient`
  ADD CONSTRAINT `FK_contient_animal` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id_animal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_contient_image` FOREIGN KEY (`id_image`) REFERENCES `image` (`id_image`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `dispose`
--
ALTER TABLE `dispose`
  ADD CONSTRAINT `FK_animal_dispose` FOREIGN KEY (`id_animal`) REFERENCES `animal` (`id_animal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_caractere_dispose` FOREIGN KEY (`id_caractere`) REFERENCES `caractere` (`id_caractere`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
